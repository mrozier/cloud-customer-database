"""cloud_customer_database URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from customer_database.api.views import LocationViewSet, PromotionViewSet, RewardViewSet, IDLinkViewSet, LogEntryViewSet, CustomerViewSet, CustomCustomerLookupListAPIView, OrderViewSet, OrderItemViewSet, OrderOptionViewSet

router = routers.DefaultRouter()
router.register(r'customer', CustomerViewSet, base_name='customer')
router.register(r'location', LocationViewSet, base_name='location')
router.register(r'promotion', PromotionViewSet, base_name='promotion')
router.register(r'reward', RewardViewSet, base_name='reward')
router.register(r'idlink', IDLinkViewSet, base_name='idlink')
router.register(r'logentry', LogEntryViewSet, base_name='logentry')
router.register(r'order', OrderViewSet, base_name='order')
router.register(r'orderitem', OrderItemViewSet, base_name='orderitem')
router.register(r'orderoption', OrderOptionViewSet, base_name='orderoption')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('customlookup', CustomCustomerLookupListAPIView.as_view()),
]
