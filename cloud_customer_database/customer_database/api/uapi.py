import requests
from django.conf import settings

api_url = 'http://api.ordercounter.com/api/'
"""
api_url = 'http://dev.api.ordercounter.com/api/'
if not settings.DEBUG:
    api_url = 'http://api.ordercounter.com/api/'
"""

class UAPI:

    api_token = None
    location_id = None

    def authenticate(self, api_id = None, api_key = None):
        post_data = {
            'route': 'login'
        }

        if api_id is not None:
            post_data['data[api_id]'] = api_id
            post_data['data[api_key]'] = api_key

        r = self.__sendPost('auth.php', post_data)
        data = r.json()

        if data['success']:
            self.api_token = data['data']['token']
            self.location_id = data['data']['locationID']
        else:
            raise Exception('UAPI Auth Failed')

    def is_authenticated(self):
        post_data = {
            'route': 'authenticated'
        }

        r = self.__sendPost('auth.php', post_data)
        data = r.json()

        return data['success']

    def reauthenticate(self):
        if not self.is_authenticated():
            self.authenticate()

    def unauthenticate(self):
        if not self.is_authenticated():
            return

        post_data = {
            'route': 'logout'
        }

        r = self.__sendPost('auth.php', post_data)
        data = r.json()

        if data['success']:
            self.api_token = None
            self.location_id = None
        else:
            raise Exception('UAPI Auth Failed')

    def get_location_info(self, loc_id):
        self.reauthenticate()

        post_data = {
            'route': 'getLocation',
            'data[locID]': loc_id
        }
        r = self.__sendPost('locations.php', post_data)
        data = r.json()

        if data['success']:
            return data
        else:
            raise Exception('Get Location for location id ' + str(loc_id) + ' failed: ' + data['error']['desc'])

    def get_location_api_info(self, loc_id):
        loc_info = self.get_location_info(loc_id)

        if loc_info['success'] and 'api_id' in loc_info['data'] and 'api_key' in loc_info['data']:
            return {
                'api_id': loc_info['data']['api_id'],
                'api_key': loc_info['data']['api_key']
            }
        else:
            raise Exception('Get Location API Info failed')

    def get_location_remote_back_office_info(self, loc_id):
        self.unauthenticate()
        api_info = self.get_location_api_info(loc_id)

        # be sure to unauthenticate so we can authenticate as the location
        self.unauthenticate()
        self.authenticate(api_info['api_id'], api_info['api_key'])

        # get the data
        post_data = {
            'route': 'getOcposSettings'
        }

        r = self.__sendPost('locations.php', post_data)
        data = r.json()

        if data['success']:
            return data['data']
        else:
            raise Exception('Get Location Settings Info Failed')

    def grant_firewall_access(self, loc_id, ip_address, start_time, end_time):
        self.unauthenticate()
        api_info = self.get_location_api_info(loc_id)

        # be sure to unauthenticate so we can authenticate as the location
        self.unauthenticate()
        self.authenticate(api_info['api_id'], api_info['api_key'])

        # get the data
        post_data = {
            'route': 'defaultRoute',
            'endpoint': 'security',
            'action': 'allowIp',
            'data[api_id]': api_info['api_id'],
            'data[api_key]': api_info['api_key'],
            'data[ip_address]': ip_address,
            'data[start_time]': start_time.strftime("%Y-%m-%d %H:%M:%S"),
            'data[end_time]': end_time.strftime("%Y-%m-%d %H:%M:%S"),
        }

        r = self.__sendPost('ocpos.php', post_data)
        data = r.json()

        if data['success']:
            return data['data']
        else:
            raise Exception('Grant Remote Access Failed')

    def __sendPost(self, endpoint, data):

        url = api_url + endpoint
        # add in common data
        data['tracking'] = 'na'

        if 'data[api_id]' not in data:
            data['data[api_id]'] = settings.UAPI_ID
            data['data[api_key]'] = settings.UAPI_KEY

        if self.api_token is not None:
            data['token'] = self.api_token

        # convert data to tuples with None so it sends request properly with multipart formdata
        new_entries = {}
        for key, value in data.items():
            if key == 'data' and type(value) is dict:
                for dk, dv in value.items():
                    new_entries['data[' + dk + ']'] = (None, str(dv))
            else:
                data[key] = (None, str(value))

        # reconcile the two dicts
        if len(new_entries) > 0:
            data.pop('data', None)

        data = {**data, **new_entries}
        return requests.post(url, files=data)

    def getTable(self, loc_id, table_name):
        self.reauthenticate()
        post_data = {
            'route':'getTable',
            'data[table_name]':table_name
        }

        r = self.__sendPost('dataTransfer.php', post_data)
        data = r.json()

        if data['success']:
            return data
        else:
            raise Exception('Get Table for location id ' + str(loc_id) + ' failed: ' + data['error']['desc'])
