from rest_framework import viewsets, filters
from rest_framework.views import APIView
from rest_framework import generics
from customer_database.models import Customer, Location, Promotion, Reward, IDLink, LogEntry, Order, OrderItem, OrderOption
from .serializers import CustomerSerializer, LocationSerializer, PromotionSerializer, RewardSerializer, IDLinkSerializer, LogEntrySerializer, OrderSerializer, OrderItemSerializer, OrderOptionSerializer
from rest_framework import authentication, permissions
from rest_framework.response import Response
import re
from django_filters import rest_framework as djangofilters

class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    lookup_field = 'id'
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'last_name', '$phone_number', 'email_address')

    def get_queryset(self):
        queryset = Customer.objects.all()
        #Filters
        #First Name
        first_name = self.request.query_params.get('first_name', None)
        if first_name is not None:
            queryset = queryset.filter(first_name__contains=first_name)

        #Last Name
        last_name = self.request.query_params.get('last_name', None)
        if last_name is not None:
            queryset = queryset.filter(last_name__contains=last_name)

        #Phone Number
        phone_number = self.request.query_params.get('phone_number', None)
        if phone_number is not None:
            phone_number = re.sub(r"[^\d]", "", phone_number)
            regex_str = ""
            for digit in phone_number:
                regex_str += digit + "[^\d]*"

            queryset = queryset.filter(phone_number__iregex=r'^[^\d]*'+regex_str+'')

        #Email Address
        email_address = self.request.query_params.get('email_address', None)
        if email_address is not None:
            queryset = queryset.filter(email_address__contains=email_address)

        return queryset

class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    lookup_field = 'location_id'

class PromotionViewSet(viewsets.ModelViewSet):
    queryset = Promotion.objects.all()
    serializer_class = PromotionSerializer
    lookup_field = 'id'

class RewardViewSet(viewsets.ModelViewSet):
    queryset = Reward.objects.all()
    serializer_class = RewardSerializer
    lookup_field = 'id'

class IDLinkViewSet(viewsets.ModelViewSet):
    queryset = IDLink.objects.all()
    serializer_class = IDLinkSerializer
    lookup_field = 'id'

class LogEntryViewSet(viewsets.ModelViewSet):
    queryset = LogEntry.objects.all()
    serializer_class = LogEntrySerializer
    lookup_field = 'id'

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    lookup_field = 'id'

class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    lookup_field = 'id'

class OrderOptionViewSet(viewsets.ModelViewSet):
    queryset = OrderOption.objects.all()
    serializer_class = OrderOptionSerializer
    lookup_field = 'id'

class CustomerFilter(djangofilters.FilterSet):
    class Meta:
        model = Customer
        fields = {
            'first_name':['exact', 'iexact', 'contains', 'icontains'],
            'last_name':['exact', 'iexact', 'contains', 'icontains'],
            'phone_number':['exact', 'iexact', 'contains', 'icontains'], #TODO
            'email_address':['exact', 'iexact', 'contains', 'icontains'],
            'orders__order_date':['lt', 'gt', 'exact', 'iexact',],
            'orders__order_items__item_id':['exact'],
        }

class CustomCustomerLookupListAPIView(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_backends = (djangofilters.DjangoFilterBackend,)
    filterset_class = CustomerFilter
