from rest_framework import serializers
from customer_database.models import Customer, Location, Promotion, Reward, IDLink, LogEntry, Message, MessageStatus, Order, OrderItem, OrderOption

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = [
            'id',
            'first_name',
            'last_name',
            'phone_number',
            'email_address',
            'points',
            'subscribed_email',
            'subscribed_sms',
            'date_created',
            'date_last_modified',
            'pos_modified',
        ]

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = [
            'id',
            'uapi_id',
            'location_name',
            'point_method',
            'point_modifier',
        ]

class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Promotion
        fields = [
            'id',
            'location',
            'coupon_id',
            'title',
            'details',
            'code',
            'type',
            'type_variable',
            'type_per',
            'condition_coup',
            'condition_variable',
            'condition_amount',
            'num_uses',
            'num_used',
            'userID',
            'restaurant_id',
            'expiration_date',
            'other_specials',
            'start_date',
            'manager',
            'multiple',
            'maxvalue',
            'vip',
            'cat_id',
            'taxable',
            'auto_add_multiple',
            'dine_in',
            'takeout',
            'delivery',
            'tables',
            'bar',
            'for_here',
            'to_go',
            'drive',
            'terminal_select',
            'terminals',
            'application_order'            
        ]

class RewardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reward
        fields = [
            'id',
            'promotion',
            'customer',
            'date_added',
            'uses_remaining',
            'date_used',
        ]

class IDLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = IDLink
        fields = [
            'id',
            'customer',
            'location',
            'pos_id',
            'oo_id',
        ]

class LogEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = LogEntry
        fields = [
            'id',
            'date',
            'message',
            'type',
        ]

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            'id',
            'message'
        ]

class MessageStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageStatus
        fields = [
            'id',
            'message_id',
            'customer_id',
            'status',
            'datetime_sent'
        ]

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'


class OrderOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderOption
        fields = '__all__'
