from django.apps import AppConfig


class CustomerDatabaseConfig(AppConfig):
    name = 'customer_database'
