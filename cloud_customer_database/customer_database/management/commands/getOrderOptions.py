from django.core.management.base import BaseCommand, CommandError
from customer_database.api.uapi import UAPI
from customer_database.models import Location, Order, OrderOption
import requests, pprint, time

class Command(BaseCommand):
    help = 'Gets a JSON string of order item data.'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        querySet = Location.objects.all()
        if querySet.count() == 0:
            Location.objects.create(uapi_id=1498, location_name='Test Database', point_method='M', point_modifier=100)
            querySet = Location.objects.all()

        for l in querySet:
            location_id = l.uapi_id
            masterUAPI = UAPI()
            apiInfo = masterUAPI.get_location_api_info(location_id)

            apiID = apiInfo['api_id']
            apiKey = apiInfo['api_key']

            masterUAPI.authenticate(api_id=apiID,api_key=apiKey)

            r = masterUAPI.getTable(table_name="pending_options", loc_id=location_id)
            if r['success']:
                if r['data']['result']:
                    for p_option in r['data']['data']:
                        loc = Location.objects.get(uapi_id=location_id)
                        try:
                            existing_order = OrderOption.objects.get(location=loc, pos_unique_id=p_option['unique_id'])
                        except OrderOption.DoesNotExist:
                            try:
                                order = Order.objects.get(location=loc, invoice_id=p_option['invoice_id'])
                                new_order_option = OrderOption.objects.create(
                                    location = loc,
                                    pos_unique_id = p_option['unique_id'],
                                    pos_id = p_option['id'],
                                    option_id = p_option['optionID'],
                                    order=order,
                                    option_name = p_option['option_name'],
                                    status = p_option['status'],
                                    price = p_option['price'],
                                    qty_mod = p_option['qty_mod'],
                                    pos_modified = return_valid_datetime(p_option['modified']),
                                )
                            except Order.DoesNotExist:
                                pass
                else:
                    print("API call failed. TODO: Descriptive message.")
            else:
                print("API call failed. TODO: Descriptive message.")

def convert_from_ocpos_datetime_to_python(datetimestring):
    try:
        python_datetime = time.strptime(datetimestring, "%Y%m%d%H%M%S")
    except Exception:
        return '2000-01-01 00:00:00'

    try:
        new_datetimestring = time.strftime("%Y-%m-%d %T", python_datetime)
    except Exception:
        return '2000-01-01 00:00:00'

    return new_datetimestring

def return_valid_datetime(datetimestring):
    try:
        datetime = time.strptime(datetimestring, "%Y-%m-%d %T")
    except Exception:
        return '2000-01-01 00:00:00'

    return datetimestring
