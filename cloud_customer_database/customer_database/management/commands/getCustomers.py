from django.core.management.base import BaseCommand, CommandError
from customer_database.api.uapi import UAPI
from customer_database.models import Location, Customer, IDLink
import requests, pprint

class Command(BaseCommand):
    help = 'Gets a JSON string of customer data.'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        querySet = Location.objects.all()
        if querySet.count() == 0:
            Location.objects.create(uapi_id=1498, location_name='Test Database', point_method='M', point_modifier=100)
            querySet = Location.objects.all()

        for l in querySet:
            location_id = l.uapi_id
            masterUAPI = UAPI()
            apiInfo = masterUAPI.get_location_api_info(location_id)

            apiID = apiInfo['api_id']
            apiKey = apiInfo['api_key']

            masterUAPI.authenticate(api_id=apiID,api_key=apiKey)

            r = masterUAPI.getTable(table_name="customers", loc_id=location_id)
            if r['success']:
                if r['data']['result']:
                    for customer in r['data']['data']:
                        loc = Location.objects.get(uapi_id=location_id)
                        try:
                            existing_cust_id = IDLink.objects.get(location=loc, pos_id=customer['customer_id'])
                        except IDLink.DoesNotExist:
                            new_cust = Customer.objects.create(
                                first_name=customer['fname'],
                                last_name=customer['lname'],
                                phone_number=customer['phone'],
                                email_address=customer['email'],
                                pos_modified=customer['modified']
                            )
                            newIDLink = IDLink.objects.create(
                                location=loc,
                                customer=new_cust,
                                pos_id=customer['customer_id']
                            )
                else:
                    print("API call failed. TODO: Descriptive message.")
            else:
                print("API call failed. TODO: Descriptive message.")
