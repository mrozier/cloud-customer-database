from django.core.management.base import BaseCommand, CommandError
from customer_database.api.uapi import UAPI
from customer_database.models import Location, Customer, Order
import requests, pprint, time

class Command(BaseCommand):
    help = 'Gets a JSON string of order data.'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        querySet = Location.objects.all()
        if querySet.count() == 0:
            Location.objects.create(uapi_id=1498, location_name='Test Database', point_method='M', point_modifier=100)
            querySet = Location.objects.all()

        for l in querySet:
            location_id = l.uapi_id
            masterUAPI = UAPI()
            apiInfo = masterUAPI.get_location_api_info(location_id)

            apiID = apiInfo['api_id']
            apiKey = apiInfo['api_key']

            masterUAPI.authenticate(api_id=apiID,api_key=apiKey)

            r = masterUAPI.getTable(table_name="orders", loc_id=location_id)
            if r['success']:
                if r['data']['result']:
                    for order in r['data']['data']:
                        loc = Location.objects.get(uapi_id=location_id)
                        try:
                            existing_order = Order.objects.get(location=loc, invoice_id=order['invoice_id'])
                        except Order.DoesNotExist:
                            try:
                                customer = Customer.objects.get(id_links__location=loc, id_links__pos_id=order['customer_id'])
                                new_order = Order.objects.create(
                                    location=loc,
                                    invoice_id=order['invoice_id'],
                                    order_id=order['order_id'],
                                    #online_invoice=order['online_invoice'],
                                    customer=customer,
                                    order_date=convert_from_ocpos_datetime_to_python(order['order_date']),
                                    order_datetime=return_valid_datetime(order['order_datetime']),
                                    ready_date=convert_from_ocpos_datetime_to_python(order['ready_date']),
                                    pos_modified=return_valid_datetime(order['modified']),
                                    subtotal=order['subtotal'],
                                    discount=order['discount'],
                                    taxtotal=order['taxtotal'],
                                    grand_total=order['grand_total'],
                                    cash_paid=order['cash_paid'],
                                    credit_paid=order['credit_paid'],
                                    check_paid=order['check_paid'],
                                    giftcard_paid=order['giftcard_paid'],
                                    giftcert_paid=order['giftcert_paid'],
                                    tips=order['tips'],
                                    gratuity=order['gratuity'],
                                    status=order['status'],
                                    method=order['method'],
                                )
                            except Customer.DoesNotExist:
                                pass #TODO: Return error message
                else:
                    print("API call failed. TODO: Descriptive message.")
            else:
                print("API call failed. TODO: Descriptive message.")

def convert_from_ocpos_datetime_to_python(datetimestring):
    try:
        python_datetime = time.strptime(datetimestring, "%Y%m%d%H%M%S")
    except Exception:
        return '2000-01-01 00:00:00'

    try:
        new_datetimestring = time.strftime("%Y-%m-%d %T", python_datetime)
    except Exception:
        return '2000-01-01 00:00:00'

    return new_datetimestring

def return_valid_datetime(datetimestring):
    try:
        datetime = time.strptime(datetimestring, "%Y-%m-%d %T")
    except Exception:
        return '2000-01-01 00:00:00'

    return datetimestring
