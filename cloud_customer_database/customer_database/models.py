from django.db import models
from encrypted_model_fields.fields import EncryptedCharField

class Location(models.Model):
    uapi_id = models.IntegerField(primary_key=True, unique=True, db_index=True)
    location_name = models.CharField(max_length=250, default='')
    point_method = models.CharField(max_length=1,default='M') #M: Money spent, V: Visits
    point_modifier = models.IntegerField(default=0) #Points per method activity

class Customer(models.Model):
    #Basic information
    first_name = models.CharField(max_length=255, default='')
    last_name = models.CharField(max_length=255, default='')
    phone_number = models.CharField(max_length=255, default='')
    email_address = models.CharField(max_length=255, default='')
    #Loyalty and marketing
    points = models.IntegerField(default=0)
    subscribed_email = models.BooleanField(default=False)
    subscribed_sms = models.BooleanField(default=False)
    #Logging info
    date_created = models.DateTimeField(auto_now_add=True)
    date_last_modified = models.DateTimeField(auto_now=True)
    pos_modified = models.DateTimeField(default='1999-01-01')

#coupons_advanced table from POS + location ID
class Promotion(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='promotions')
    coupon_id = models.IntegerField(default=0)
    title = models.CharField(max_length=255, default='')
    details = models.TextField(default='')
    code = models.CharField(max_length=40, default='')
    type = models.CharField(max_length=2, default='')
    type_variable = models.TextField(default='')
    type_per = models.IntegerField(default=0)
    condition_coup = models.CharField(max_length=2, default='')
    condition_variable = models.TextField(default='')
    condition_amount = models.IntegerField(default=0)
    num_uses = models.IntegerField(default=0)
    num_used = models.IntegerField(default=0)
    userID = models.IntegerField(default=0)
    restaurant_id = models.IntegerField(default=0)
    expiration_date = models.CharField(max_length=8, default='')
    other_specials = models.CharField(max_length=1, default='')
    start_date = models.CharField(max_length=8, default='')
    manager = models.CharField(max_length=1, default='')
    multiple = models.CharField(max_length=1, default='')
    maxvalue = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    vip = models.CharField(max_length=1, default='')
    cat_id = models.IntegerField(default=0)
    taxable = models.CharField(max_length=1, default='')
    auto_add_multiple = models.CharField(max_length=1, default='')
    dine_in = models.CharField(max_length=1, default='')
    takeout = models.CharField(max_length=1, default='')
    delivery = models.CharField(max_length=1, default='')
    tables = models.CharField(max_length=1, default='')
    bar = models.CharField(max_length=1, default='')
    for_here = models.CharField(max_length=1, default='')
    to_go = models.CharField(max_length=1, default='')
    drive = models.CharField(max_length=1, default='')
    terminal_select = models.CharField(max_length=1, default='')
    terminals = models.TextField(default='')
    application_order = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)

class Reward(models.Model):
    promotion = models.IntegerField(default=0)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='rewards')
    date_added = models.DateTimeField(auto_now_add=True)
    uses_remaining = models.IntegerField(default=0)
    date_used = models.DateTimeField()

class IDLink(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='id_links')
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='id_links')
    pos_id = models.IntegerField(default=0)
    oo_id = models.IntegerField(default=0)

class LogEntry(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    message = models.TextField(default='')
    type = models.CharField(max_length=300, default='')

class RewardActivity(models.Model):
    pass #TODO

class Message(models.Model):
    message = models.TextField(default='')

class MessageStatus(models.Model):
    message = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='message_status')
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, default='N') #(S)ent, (F)ailed to send, (N)ot Sent, etc.
    datetime_sent = models.DateTimeField(null=True)

class Order(models.Model):
    #IDs
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='orders')
    invoice_id = models.IntegerField(unique=True)
    order_id = models.IntegerField()
    #online_invoice = models.CharField(max_length=)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='orders')
    #Dates
    order_date = models.DateTimeField()
    order_datetime = models.DateTimeField()
    ready_date = models.DateTimeField()
    pos_modified = models.DateTimeField(default='1999-01-01')
    #Money
    subtotal = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    discount = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    taxtotal = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    grand_total = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    cash_paid = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    credit_paid = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    check_paid = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    giftcard_paid = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    giftcert_paid = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    tips = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    gratuity = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    #Metadata
    status = models.CharField(max_length=1)
    method = models.CharField(max_length=1)

class OrderItem(models.Model):
    #IDs
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='order_items')
    pos_unique_id = models.IntegerField()
    pos_id = models.IntegerField()
    item_id = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items')
    #Metadata
    item_name = models.CharField(max_length=255, default='')
    status = models.CharField(max_length=1)
    price = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    quantity = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    #Dates
    pos_modified = models.DateTimeField()

class OrderOption(models.Model):
    # IDs
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='order_options')
    pos_unique_id = models.IntegerField()
    pos_id = models.IntegerField()
    option_id = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_options')
    # Metadata
    option_name = models.CharField(max_length=255, default='')
    status = models.CharField(max_length=1)
    price = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    qty_mod = models.DecimalField(default=0.0, decimal_places=2, max_digits=8)
    # Dates
    pos_modified = models.DateTimeField()
