$(function() {
    $('.clickable-row').click(function(e) {
        var element = $(e.currentTarget);
        var href = element.data('href')
        var newWindow = element.data('window');
        if (newWindow) {
            window.open(href);
        } else {
            window.location = href;
        }
    });
});
