#!/bin/bash

sudo -u postgres createuser vagrant
sudo -u postgres psql -c 'ALTER USER vagrant CREATEDB'
sudo -u postgres psql -c "ALTER USER vagrant PASSWORD 'vagrant'"
psql cloud_customer_database -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'cloud_customer_database' AND pid <> pg_backend_pid()"
dropdb cloud_customer_database --if-exists
createdb -O vagrant cloud_customer_database
