#!/bin/bash

/vagrant/scripts/recreatedb.sh
heroku pg:backups capture --app dev-cloud_customer_database
heroku pg:backups download --app dev-cloud_customer_database
PGPASSWORD=vagrant pg_restore --verbose --clean --no-acl --no-owner -h localhost -U vagrant -d cloud_customer_database latest.dump
rm latest.dump
